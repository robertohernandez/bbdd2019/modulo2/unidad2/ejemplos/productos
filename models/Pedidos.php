<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pedidos".
 *
 * @property int $idPedido
 * @property int $producto
 * @property int $tienda
 * @property double $precioCompra
 * @property int $cantidad
 * @property string $fechaPedido
 * @property string $horaPedido
 *
 * @property Tienda $tienda0
 * @property Producto $producto0
 */
class Pedidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pedidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['producto', 'tienda', 'cantidad'], 'integer'],
            [['precioCompra'], 'number'],
            [['fechaPedido', 'horaPedido'], 'safe'],
            [['producto', 'tienda', 'fechaPedido', 'horaPedido'], 'unique', 'targetAttribute' => ['producto', 'tienda', 'fechaPedido', 'horaPedido']],
            [['tienda'], 'exist', 'skipOnError' => true, 'targetClass' => Tienda::className(), 'targetAttribute' => ['tienda' => 'idTienda']],
            [['producto'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['producto' => 'idproducto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idPedido' => 'Id Pedido',
            'producto' => 'Producto',
            'tienda' => 'Tienda',
            'precioCompra' => 'Precio Compra',
            'cantidad' => 'Cantidad',
            'fechaPedido' => 'Fecha Pedido',
            'horaPedido' => 'Hora Pedido',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTienda0()
    {
        return $this->hasOne(Tienda::className(), ['idTienda' => 'tienda']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducto0()
    {
        return $this->hasOne(Producto::className(), ['idproducto' => 'producto']);
    }
}
