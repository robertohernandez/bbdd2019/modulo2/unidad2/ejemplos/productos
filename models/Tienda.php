<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tienda".
 *
 * @property int $idTienda
 * @property string $direccion
 * @property string $poblacion
 * @property string $telefono
 * @property string $email
 *
 * @property Pedidos[] $pedidos
 * @property Stock[] $stocks
 * @property Producto[] $productos
 */
class Tienda extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tienda';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['direccion'], 'string', 'max' => 100],
            [['poblacion', 'email'], 'string', 'max' => 50],
            [['telefono'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idTienda' => 'Id Tienda',
            'direccion' => 'Direccion',
            'poblacion' => 'Poblacion',
            'telefono' => 'Telefono',
            'email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidos()
    {
        return $this->hasMany(Pedidos::className(), ['tienda' => 'idTienda']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStocks()
    {
        return $this->hasMany(Stock::className(), ['tienda' => 'idTienda']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Producto::className(), ['idproducto' => 'producto'])->viaTable('stock', ['tienda' => 'idTienda']);
    }
}
